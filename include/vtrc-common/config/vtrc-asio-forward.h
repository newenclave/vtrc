#ifndef VTRC_ASIO_FORWARD_H
#define VTRC_ASIO_FORWARD_H

#define VTRC_ASIO_FORWARD( x ) namespace boost { namespace asio { x } }

#define VTRC_ASIO ::boost::asio

#endif // VTRC_VTRC_ASIO_FORWARD_H

