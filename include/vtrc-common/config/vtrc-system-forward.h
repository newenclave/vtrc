#ifndef VTRC_SYSTEM_FORWARD_H
#define VTRC_SYSTEM_FORWARD_H

#define VTRC_SYSTEM_FORWARD( x ) namespace boost { namespace system { x } }

#define VTRC_SYSTEM ::boost::system

#endif // VTRC_SYSTEM_FORWARD_H

